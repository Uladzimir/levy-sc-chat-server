package com.company;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static java.lang.Thread.sleep;

public class ChatServer {



    private List<String> acceptedMessage = new ArrayList<>();
    private ServerSocket serverSocket;
    private Queue<PrintWriter> writers = new ConcurrentLinkedQueue<>();
    private static String CHAT_HISTORY = "d:\\JAVA\\STUDY\\ConsoleServer\\chat_history.txt";

    public void start(){
        try {
            serverSocket = new ServerSocket(4242);

            Thread threadSender = new Thread(new Sender());
            threadSender.start();
            while (true) {
                Socket socket = serverSocket.accept();

                if (socket != null) {
                    PrintWriter writer = new PrintWriter(socket.getOutputStream());
                    writers.add(writer);

                    InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
                    BufferedReader reader = new BufferedReader(streamReader);

                    String message;
                    if ((message = reader.readLine()) != null) {
                        acceptedMessage.add(message);
                    }

                    Thread threadListner = new Thread(new Listner(socket));
                    threadListner.start();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class Sender implements Runnable {

        @Override
        public void run() {
            try {
//                PrintWriter writer = new PrintWriter(socket.getOutputStream());
                while (true) {
                    for(PrintWriter writer: writers) {
                        for (int i = 0; i < acceptedMessage.size(); i++) {
                            if (acceptedMessage.get(i).length() > 0) {
                                writer.println(acceptedMessage.get(i));
                                writer.flush();
                                sleep(100);
                            }
                        }
                    }
                    acceptedMessage.clear();
                    sleep(5000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public class Listner implements Runnable {

        private Socket socket;

        public Listner(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            try {
                InputStreamReader streamReader = new InputStreamReader(socket.getInputStream());
                BufferedReader reader = new BufferedReader(streamReader);

                String message;
                while(true) {
                    if ((message = reader.readLine()) != null) {
                        acceptedMessage.add(message);
                        System.out.println("Income: " + message);
                        try (FileWriter writer = new FileWriter(CHAT_HISTORY, true)) {
                            writer.write(message + " ");
                            writer.append('\n');
                        }
                        catch (IOException exception){
                            System.out.println(exception.getMessage());
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();

                try {
                    socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                socket = null;
            }
        }
    }

}
